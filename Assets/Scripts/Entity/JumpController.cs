﻿using System;
using UnityEngine;

public class JumpController : MonoBehaviour
{
	[SerializeField] private Rigidbody _rigidbody;
	[SerializeField] private float _jumpSpeed;
	[SerializeField] private GroundChecker _groundChecker;

	private float _doJump = 0.0f;

	public void Jump()
	{
		_doJump = Time.time + 0.1f;
	}

	private void FixedUpdate()
	{
		if(_doJump > Time.time)
			DoJump();
	}

	private void DoJump()
	{
		if (!_groundChecker.Grounded)
			return;

		_doJump = 0.0f;
		var curr = _rigidbody.velocity;
		curr.y = _jumpSpeed;
		_rigidbody.AddForce(curr, ForceMode.VelocityChange);
	}
}
