﻿using System;
using UnityEngine;

public class MoveController : MonoBehaviour
{
	[SerializeField] private Rigidbody _rigidBody;
	[SerializeField] private float _speed;
	[SerializeField] private Transform _rotateables;
	[SerializeField] private Transform _lookAt;

	public float Speed { get => _speed; set => _speed = value; }

	public Vector3 MoveDirection { get; set; }

	public void FixedUpdate()
	{
		Move();
		Rotate();
	}

	private void Move()
	{
		var dir = MoveDirection.x;
		if(dir == 0.0f)
		{
			dir = _rigidBody.velocity.x > 0.0f ? 0.000001f : -0.000001f;
		}
		var newVelocity = new Vector3(Speed * dir, _rigidBody.velocity.y, .0f);
		_rigidBody.velocity = newVelocity;
	}

	private void Rotate()
	{
		_lookAt.localPosition = new Vector3( 0.0f, 0.0f, _rigidBody.velocity.normalized.x);
		_rotateables.LookAt(_lookAt, _rotateables.up);
	}
}
