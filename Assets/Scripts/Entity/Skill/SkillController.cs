﻿using System;
using UnityEngine;

public class SkillController : MonoBehaviour
{
	[SerializeField] private Fireball _fireballPrefab;
	[SerializeField] private Transform _launchPosition;
	[SerializeField] private float _fireballCooldown;

	private float _fireballOnCooldownTimeEnd = 0.0f;

	public enum eSkillType
	{
		Fireball,
	}

	public void Cast(eSkillType skillType)
	{
		switch(skillType)
		{
			case eSkillType.Fireball:
				CastFireBall();
				break;
		}
	}

	private void CastFireBall()
	{
		if (!CanCastFireBall())
			return;
		var fireBall = Instantiate(_fireballPrefab);
		fireBall.Cast(_launchPosition);
		_fireballOnCooldownTimeEnd = Time.time + _fireballCooldown;
	}

	private bool CanCastFireBall()
	{
		return _fireballOnCooldownTimeEnd < Time.time;
	}
}
