﻿using UnityEngine;

public class Fireball : MonoBehaviour, ISkill
{
	[SerializeField] private Rigidbody _rigidbody;
	[SerializeField] private MoveController _moveController;
	[SerializeField] private float _damage;

	public void Cast(Transform launchPosition)
	{
		transform.position = launchPosition.position;
		transform.rotation = launchPosition.rotation;
		_moveController.MoveDirection = transform.forward;
		gameObject.SetActive(true);
	}

	public void DoReset()
	{
		gameObject.SetActive(false);
	}

	private void OnTriggerEnter(Collider other)
	{
		var damageable = other.GetComponent<IDamageable>();
		if (damageable != null)
			damageable.TakeDamage(_damage);
	}
}
