﻿
public interface IDamageable
{
	void TakeDamage(float damageable);
}
