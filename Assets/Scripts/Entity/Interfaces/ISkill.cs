﻿using UnityEngine;

public interface ISkill
{
	void Cast(Transform launchPosition);
	void DoReset();
}
