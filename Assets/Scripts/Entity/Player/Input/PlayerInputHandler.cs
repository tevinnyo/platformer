﻿using System;
using UnityEngine;
using eSkillType = SkillController.eSkillType;

public class PlayerInputHandler : MonoBehaviour
{
	public Action<Vector2> OnMoveInputChanged;
	public Action<bool> OnJumpInputChanged;
	public Action<eSkillType, bool> OnSkillPressed;

	private Vector2 _prevMove = Vector2.positiveInfinity;

	public void Update()
	{
		CheckMoveInputs();
		CheckJumpInputs();
		CheckShotInputs();
	}

	private void CheckMoveInputs()
	{
		var moveHorizontal = Input.GetAxis("Horizontal");
		var moveVertical = Input.GetAxis("Vertical");
		var currentMove = new Vector2(moveHorizontal, moveVertical);
		if (currentMove == _prevMove)
			return;
		OnMoveInputChanged?.Invoke(currentMove);
		_prevMove = currentMove;
	}

	private void CheckJumpInputs()
	{
		if( Input.GetButtonDown("Jump"))
		{
			OnJumpInputChanged?.Invoke(true);
		}
		else if( Input.GetButtonUp("Jump"))
		{
			OnJumpInputChanged?.Invoke(false);
		}
	}

	private void CheckShotInputs()
	{
		if (Input.GetButtonDown("Fire1"))
		{
			OnSkillPressed?.Invoke(eSkillType.Fireball, true);
		}
		else if (Input.GetButtonUp("Fire1"))
		{
			OnSkillPressed?.Invoke(eSkillType.Fireball, false);
		}
	}
}
