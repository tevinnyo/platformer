﻿using System;
using UnityEngine;

public class Player : Singleton<Player>, IDamageable
{
	#region members
	[SerializeField] private PlayerInputHandler _inputHandler;
	[SerializeField] private MoveController _moveController;
	[SerializeField] private JumpController _jumpController;
	[SerializeField] private SkillController _skillController;

	[SerializeField] private float _health;

	private float _maxHealth;
	#endregion

	protected override void Awake()
	{
		base.Awake();
		_maxHealth = _health;
		RegisterOnInputHandler();
	}

	protected override void OnDestroy()
	{
		base.OnDestroy();
		UnregisterOnInputHandler();
	}

	private void OnMoveInputChanged(Vector2 move)
	{
		_moveController.MoveDirection = move.normalized;
	}

	internal void RespawnPlayer(Vector3 playerStartPosition)
	{
		transform.position = playerStartPosition;
		_moveController.MoveDirection = Vector2.zero;
		_health = _maxHealth;
	}

	private void OnJumpInputChanged(bool jump)
	{
		if(jump)
			_jumpController.Jump();
	}

	private void OnSkillPressed(SkillController.eSkillType skillType, bool pressed)
	{
		if(pressed)
		{
			_skillController.Cast(skillType);
		}
	}

	public void TakeDamage(float damageable)
	{
		_health -= damageable;
		if(_health < 0.0f)
		{
			LevelManager.Instance.PlayerDied();
		}
	}

	private void RegisterOnInputHandler()
	{
		_inputHandler.OnMoveInputChanged += OnMoveInputChanged;
		_inputHandler.OnJumpInputChanged += OnJumpInputChanged;
		_inputHandler.OnSkillPressed += OnSkillPressed;
	}

	private void UnregisterOnInputHandler()
	{
		_inputHandler.OnMoveInputChanged -= OnMoveInputChanged;
		_inputHandler.OnJumpInputChanged -= OnJumpInputChanged;
		_inputHandler.OnSkillPressed -= OnSkillPressed;
	}
}
