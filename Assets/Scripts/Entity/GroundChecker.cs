﻿using UnityEngine;

public class GroundChecker : MonoBehaviour
{
	private int _groundCounter = 0;

	public bool Grounded { get => _groundCounter > 0; }

	private void OnTriggerEnter(Collider other)
	{
		_groundCounter += 1;	
	}

	private void OnTriggerExit(Collider other)
	{
		_groundCounter -= 1;
		if(_groundCounter < 0)
		{
			Debug.LogError("Valami gond van a groundeddel");
		}
	}
}
