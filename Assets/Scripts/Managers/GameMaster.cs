﻿using UnityEngine;
using UnityEngine.EventSystems;

public class GameMaster : Singleton<GameMaster>
{
	[SerializeField] private SceneManager _sceneManager;
	[SerializeField] private EventSystem _eventSystem;

	public EventSystem EventSystem { get => _eventSystem; }

	protected override void Awake()
	{
		if (!SingletonCheck())
			return;
		DontDestroyOnLoad(gameObject);
		if(LevelManager.Instance == null)
			_sceneManager.LoadSceneAsync("Level1");
	}

	public void GameOver()
	{
		Debug.LogError("GameOver");
		_sceneManager.OnLoadingDone += UnloadDoneHack;
		_sceneManager.UnloadSceneAsync("Level1");
	}

	private void UnloadDoneHack(int buildIndex)
	{
		_sceneManager.OnLoadingDone -= UnloadDoneHack;
		_sceneManager.LoadSceneAsync(buildIndex);
	}
}
