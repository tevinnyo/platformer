﻿using UnityEngine;

public abstract class Singleton<T> : MonoBehaviour where T : Singleton<T>
{
	public static T Instance { get; protected set; }

	protected virtual void Awake()
	{
		if (!SingletonCheck())
			return;
	}

	protected bool SingletonCheck()
	{
		if (Instance != null)
		{
			Debug.LogError("Eleg lesz egy " + GetType() + "-bol is. Uj es egyben torolt:", gameObject);
			Debug.LogError("Jelenlegi GameController: " + Instance.gameObject.name, Instance.gameObject);
			Debug.Break();
			Destroy(this);
			return false;
		}
		Instance = (T)this;
		return true;
	}

	protected virtual void OnDestroy()
	{
		Instance = null;
	}
}

