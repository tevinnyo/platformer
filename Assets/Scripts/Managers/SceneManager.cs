﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Unity_SceneManager = UnityEngine.SceneManagement.SceneManager;

public class SceneManager : MonoBehaviour
{
	[SerializeField] private LoadingScreen _loadingScene;
	[SerializeField] List<SceneBuildIndex> _sceneBuildIndeces;

	public Action<int> OnLoadingDone { get; set; }

	public void LoadSceneAsync(int buildIndex)
	{
		StartCoroutine(LoadingRoutine(buildIndex));
	}

	public void LoadSceneAsync(string sceneName)
	{
		if (!GetBuildIndex(sceneName, out var sceneBuildIndex))
			return;
		LoadSceneAsync(sceneBuildIndex.BuildIndex);
	}

	private IEnumerator LoadingRoutine(int buildIndex)
	{
		_loadingScene.gameObject.SetActive(true);
		var operation = Unity_SceneManager.LoadSceneAsync(buildIndex, LoadSceneMode.Additive);
		if (!LoadingProgress(operation))
			yield return null;
		_loadingScene.gameObject.SetActive(false);
		OnLoadingDone?.Invoke(buildIndex);
	}

	public void UnloadSceneAsync(int buildIndex)
	{
		StartCoroutine(UnloadingRoutine(buildIndex));
	}

	public void UnloadSceneAsync(string sceneName)
	{
		if (!GetBuildIndex(sceneName, out var sceneBuildIndex))
			return;
		UnloadSceneAsync(sceneBuildIndex.BuildIndex);
	}

	private bool GetBuildIndex(string sceneName, out SceneBuildIndex sceneBuildIndex)
	{
		sceneBuildIndex = _sceneBuildIndeces.Find((x) => x.Name == sceneName);
		if (sceneBuildIndex == null)
		{
			Debug.LogError("Nincsen ilyen scene: " + sceneName);
			return false;
		}
		return true;
	}

	private IEnumerator UnloadingRoutine(int buildIndex)
	{
		_loadingScene.gameObject.SetActive(true);
		var operation = Unity_SceneManager.UnloadSceneAsync(buildIndex);
		if (!LoadingProgress(operation))
			yield return null;
		_loadingScene.gameObject.SetActive(false);
		OnLoadingDone?.Invoke(buildIndex);
	}

	private bool LoadingProgress(AsyncOperation operation)
	{
		if (operation.isDone)
			return true;
		_loadingScene.Progress = operation.progress / 0.9f;
		return false;
	}

	[Serializable]
	private class SceneBuildIndex
	{
		public string Name;
		public int BuildIndex;
	}
}
