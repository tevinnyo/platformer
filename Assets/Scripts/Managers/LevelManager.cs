﻿using UnityEngine;

public class LevelManager : Singleton<LevelManager>
{
	[SerializeField] private Player _player;
	[SerializeField] private int _playerLife = int.MaxValue;

	private Vector3 _playerStartPosition;

	protected override void Awake()
	{
		base.Awake();
		_playerStartPosition = _player.transform.position;
#if UNITY_EDITOR
		if (GameMaster.Instance == null)
		{
			//Ha nincsen betoltve a GameMaster, akkor nincsen betoltve az init scene -> toltsuk be. Azert csak UNITY_EDITOR-ban, mivel standalone build mindig az init scenenel fog kezdodni.
			UnityEngine.SceneManagement.SceneManager.LoadScene(0);
		}
#endif
	}

	public void PlayerDied()
	{
		--_playerLife;
		if (_playerLife < 0.0f)
			GameMaster.Instance.GameOver();
		else
		{
			Player.Instance.RespawnPlayer(_playerStartPosition);
		}
	}

	protected override void OnDestroy()
	{
		Instance = null;
	}
}
