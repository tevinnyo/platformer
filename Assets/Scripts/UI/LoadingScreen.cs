﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class LoadingScreen : MonoBehaviour
{
	[SerializeField] private Slider _slider;
	[SerializeField] private Text _progressText;
	[SerializeField] private Text _loadingText;
	[SerializeField] private float _dotChangeFrequency = 1;

	private float _dotTimer = 0.0f;
	private int _dotCount = -1;

	private int DotCount
	{
		get => _dotCount;
		set
		{
			_dotCount = value;
			if (_dotCount > 3)
				_dotCount = 0;
			OnDotCountChanged();
		}
	}

	public float Progress
	{
		set
		{
			if (value > 1.0)
				value = 1.0f;
			_slider.value = value;
			_progressText.text = value * 100 + "%";
		}
	}

	private void OnEnable()
	{
		Progress = 0.0f;
	}

	private void Update()
	{
		if (_dotTimer < Time.time)
		{
			++DotCount;
			_dotTimer = Time.time + 1 / _dotChangeFrequency;
		}
	}

	private void OnDotCountChanged()
	{
		RefreshLoadingText();
	}

	private void RefreshLoadingText()
	{
		var loading = "Loading";
		for (int i = 0; i < _dotCount; ++i)
			loading += ".";
		_loadingText.text = loading;
	}
}
