﻿using UnityEngine;

public class DeadZone : MonoBehaviour
{
	[SerializeField] private float _damagePerSec;

	private void OnTriggerStay(Collider other)
	{
		var damageable = other.GetComponentInParent<IDamageable>();
		if (damageable != null)
			damageable.TakeDamage(_damagePerSec * Time.fixedDeltaTime);
	}
}
